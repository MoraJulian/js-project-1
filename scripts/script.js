"use strict"

document.addEventListener('DOMContentLoaded', setup);
let toDoArray = [];
let checkboxCounter = 0;

function setup(){
    document.getElementById('importance').addEventListener('input',updateDotColor);
    document.getElementById('inputPanel').addEventListener('submit',submit);
    loadToDo();
}

function submit(e){
    console.log('start of submit');
    const task = document.getElementById('task').value;
    const description = document.getElementById('description').value;
    const importance = document.getElementById('importance').value;
    let radio = document.getElementsByName('categories');
    let category;
    for(let i in radio){
        if(radio[i].checked){
            category = radio[i].value;
        }
    }
    
    let toDoObject = {
        task: task,
        description: description,
        importance: importance,
        category: category
    }
    console.log(toDoObject);

    toDoArray.push(toDoObject);

    //CREATING AND ELEMENTS
    
    let tasks = document.getElementById('taskBox');
    tasks.innerHTML += `
    <div class="task">
        <h2>${task}</h2>
        <h3>${category}</h3>
        <input type="checkbox" id="${checkboxCounter}" class="checks">
        <p>${description}</p>
        <div id="importanceBar" class="importance${importance}" >
        </div>
    </div>
    `

    checkboxEventListenerAdder();
    checkboxCounter++;

    storeInLocalStorage(); 
    e.preventDefault();
}

// Stores the toDoArray into the localStorage
function storeInLocalStorage(){
    localStorage.setItem('taskList',JSON.stringify(toDoArray));
}

// Loads all the saved tasks in LocalStorage and reloads them into the DOM/toDoArray
function loadToDo(){
    let tasks = document.getElementById('taskBox');
    if(localStorage.getItem('taskList')){
        toDoArray = JSON.parse(localStorage.getItem('taskList'));
        for(let element in toDoArray){
            tasks.innerHTML += `
            <div class="task">
                <h2>${toDoArray[element].task}</h2>
                <h3>${toDoArray[element].category}</h3>
                <input type="checkbox" id="${checkboxCounter}" class="checks">
                <p>${toDoArray[element].description}</p>
                <div id="importanceBar" class="importance${toDoArray[element].importance}" >
                </div>
            </div>
            `
            checkboxCounter++;

        }
        
    }
    checkboxEventListenerAdder();
}

// Changes the color of the Importance dot relative to the importance of the task
function updateDotColor(){  
    if(document.getElementById('importance').value==1){
        document.getElementById('importanceDot').style.backgroundColor = "white" ;
    }
    if(document.getElementById('importance').value==2){
        document.getElementById('importanceDot').style.backgroundColor = "yellow";
    }
    if(document.getElementById('importance').value==3){
        document.getElementById('importanceDot').style.backgroundColor = "#ff9a03";
    }
    if(document.getElementById('importance').value==4){
        document.getElementById('importanceDot').style.backgroundColor = "red";
    }
}

// Removes task div from the DOM and from the toDoArray
function checkedAction(element){
    toDoArray = JSON.parse(localStorage.getItem('taskList'));
    element.target.parentElement.remove();
    let positionInArray = element.target.id;
    toDoArray.splice(positionInArray,1);
    storeInLocalStorage();
}

// Adds an Event listener to every checkbox currently in the DOM
function checkboxEventListenerAdder(){
    let checkboxClass = document.getElementsByClassName('checks');
    for(let i of checkboxClass){
        i.addEventListener('click',checkedAction);
    } 
}

